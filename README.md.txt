SQL 

1. I assume the ticker is the trader symbol.
2. I assume the close is the ending trading price. 

my SQL code can be found in the SQLChallenges database.
My table name is Q2a


Total number of characters - ./sqlchallenge 1 sample_dataset2.csv

SQL Procedure - open SQLChallenges and execute exec newDate '201010110900'

### Table 

CREATE TABLE [dbo].[Q2](
	[ticker] [varchar](4) NOT NULL,
	[date] [varchar](12) NOT NULL,
	[open] [decimal](8, 4) NOT NULL,
	[high] [decimal](8, 4) NOT NULL,
	[low] [decimal](8, 4) NOT NULL,
	[close] [decimal](8, 4) NOT NULL,
	[vol] [int] NULL
) ON [PRIMARY]

###SQL Query

/****** Script for SelectTopNRows command from SSMS  ******/

--select ticker, [date], [open], [high], [low], [close],
--convert(int,vol)
--as vol
--into Q2
--from Q2a

CREATE PROCEDURE newDate
@enterDate varchar (12)
AS
-- The Volume Weighted Price

   SELECT ticker,
   SUM([close]*vol)/SUM(vol) as newDate,
   	substring(@enterDate,1,8) +' ' +
	substring (@enterDate,9,2) + ':' + substring
	(@enterDate ,11,2)  AS DATE,


'START:(' + substring(convert(varchar(12),convert(datetime,
(substring(@enterDate,1,8)+' '+
substring(@enterDate,9,2)+':'+substring(@enterDate,11,2) )),108),1,5)+
') -
END: (' + substring(convert(varchar(12), DATEADD(HH,+5, convert(datetime,
(substring(@enterDate,1,8)+' '+
substring(@enterDate,9,2)+':'+substring(@enterDate,11,2)))),108),1,5)+')' as Interval


	FROM Q2
	WHERE convert(datetime,
	(substring([date],1,8) +' ' +
	substring ([date],9,2) + ':' + substring
	(@enterDate,11,2) ))
	BETWEEN convert(datetime,
	(substring(@enterDate,1,8) +' ' +
	substring (@enterDate,9,2) + ':' + substring
	(@enterDate ,11,2) )) AND DATEADD(hh, +5,
	convert(datetime,
	(substring(@enterDate,1,8) +' ' +
	substring (@enterDate,9,2) + ':' + substring
	(@enterDate ,11,2) )))
	Group by ticker

	DROP PROCEDURE newDate

	exec newDate '201010110900'



##Challenge 2

Total number of characters - ./sqlchallenge 1 sample_dataset3.csv

SQL Procedure - open Trading and exec tRange


CREATE TABLE [dbo].[Challenge2](
	[Date] [varchar](10) NOT NULL,
	[Time] [varchar](4) NOT NULL,
	[Open] [decimal](5, 2) NOT NULL,
	[High] [decimal](5, 2) NOT NULL,
	[Low] [decimal](5, 2) NOT NULL,
	[Close] [decimal](5, 2) NOT NULL,
	[Volume] [varchar](7) NOT NULL
) ON [PRIMARY]


CREATE PROCEDURE tRange
as 
	select distinct top 3 [date],abs([open] -[close]) as tradingRange
	INTO #mytemptable
	from Challenge2
	order by tradingRange desc


	select [date],max(high) as high_Max
	into #mytemptable2
	from  Challenge2
	where [date] in (select date from #mytemptable)
	group by [date]
	order by high_Max

	
	select Challenge2.[date], [time] 
	into #mytemptable3
	from Challenge2, #mytemptable2
	where Challenge2.[High] = high_Max and Challenge2.[Date] = #mytemptable2.[Date]
	group by Challenge2.[Date], [Time]
	order by date asc


	select #mytemptable.[date] as date, #mytemptable.tradingRange, #mytemptable3.Time 'Time of Maximum Price'
	from #mytemptable left join #mytemptable3 on
	#mytemptable3.[date] = #mytemptable.[date]

		drop table #mytemptable
		drop table #mytemptable2
		drop table #mytemptable3

DROP PROCEDURE tRange

exec tRange


