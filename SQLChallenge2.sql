/****** Script for SelectTopNRows command from SSMS  ******/
--CREATE PROCEDURE tradingRanges
--@enterDate varchar(10)
--as

--select [Date],
--SUM([Open]-[Close]) as tradingRanges
--FROM Challenge2
--GROUP by [Date]
--ORDER by tradingRanges desc	

--	DROP PROCEDURE tradingRanges

--	exec tradingRanges '20101011'



CREATE PROCEDURE tRange
as 
	select distinct top 3 [date],abs([open] -[close]) as tradingRange
	INTO #mytemptable
	from Challenge2
	order by tradingRange desc


	select [date],max(high) as high_Max
	into #mytemptable2
	from  Challenge2
	where [date] in (select date from #mytemptable)
	group by [date]
	order by high_Max

	
	select Challenge2.[date], [time] 
	into #mytemptable3
	from Challenge2, #mytemptable2
	where Challenge2.[High] = high_Max and Challenge2.[Date] = #mytemptable2.[Date]
	group by Challenge2.[Date], [Time]
	order by date asc


	select #mytemptable.[date] as date, #mytemptable.tradingRange, #mytemptable3.Time 'Time of Maximum Price'
	from #mytemptable left join #mytemptable3 on
	#mytemptable3.[date] = #mytemptable.[date]

		drop table #mytemptable
		drop table #mytemptable2
		drop table #mytemptable3


		DROP PROCEDURE tRange

		exec tRange