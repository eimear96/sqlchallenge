/****** Script for SelectTopNRows command from SSMS  ******/

--select ticker, [date], [open], [high], [low], [close],
--convert(int,vol)
--as vol
--into Q2
--from Q2a

CREATE PROCEDURE newDate
@enterDate varchar (12)
AS
-- The Volume Weighted Price

   SELECT ticker,
   SUM([close]*vol)/SUM(vol) as newDate,
   	substring(@enterDate,1,8) +' ' +
	substring (@enterDate,9,2) + ':' + substring
	(@enterDate ,11,2)  AS DATE,


'START:(' + substring(convert(varchar(12),convert(datetime,
(substring(@enterDate,1,8)+' '+
substring(@enterDate,9,2)+':'+substring(@enterDate,11,2) )),108),1,5)+
') -
END: (' + substring(convert(varchar(12), DATEADD(HH,+5, convert(datetime,
(substring(@enterDate,1,8)+' '+
substring(@enterDate,9,2)+':'+substring(@enterDate,11,2)))),108),1,5)+')' as Interval


	FROM Q2
	WHERE convert(datetime,
	(substring([date],1,8) +' ' +
	substring ([date],9,2) + ':' + substring
	(@enterDate,11,2) ))
	BETWEEN convert(datetime,
	(substring(@enterDate,1,8) +' ' +
	substring (@enterDate,9,2) + ':' + substring
	(@enterDate ,11,2) )) AND DATEADD(hh, +5,
	convert(datetime,
	(substring(@enterDate,1,8) +' ' +
	substring (@enterDate,9,2) + ':' + substring
	(@enterDate ,11,2) )))
	Group by ticker

	DROP PROCEDURE newDate

	exec newDate '201010110900'

--select *
--from Q2a

--CREATE PROCEDURE newDate
--@userEnteredDateTime dataType(value)
--as
--	SELECT yourFieldSize,
--	SUM(yourSizeField*yourTradePriceField)/
--	SUM(yourSizeField) as VWAP
--	FROM Q2a
--	WHERE yourDateField BETWEEN @ @userEnteredDateTime
--	AND DATEADD(hh, +5,
--	convert(datetime,
--	(substring([date],1,8) +' ' +
--	substring ([date],9,2) + ':' + substring
--	([date],11,2) )))
--	GROUP BY yourFieldForSymbol

--	CREATE PROCEDURE


--	SELECT substring([date],1,4) +'-'+
--	substring([date],5,2)+'-'+
--	substring([date],7,2)
--	as Date,
--	'START:('substring(convert(varchar(12), convert(datetime,
--	(substring([date],1,8)+' '+
--	substring([date],9,2)+':'+substring([date],11,2) )),108),1,5)+') - END: --(' as Time
--	FROM Q2a

--	select vol
--	from Q2a